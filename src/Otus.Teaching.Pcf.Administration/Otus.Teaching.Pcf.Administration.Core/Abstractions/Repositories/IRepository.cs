﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Otus.Teaching.Pcf.Administration.Core.Domain;

namespace Otus.Teaching.Pcf.Administration.Core.Abstractions.Repositories
{
    public interface IRepository<T>
        where T: BaseEntity
    {
        Task<IEnumerable<T>> GetAllAsync();
        
        Task<T> GetByIdAsync(Guid id);
        
        Task<IEnumerable<T>> GetRangeByIdsAsync(List<Guid> ids);
        
        Task AddAsync(T entity);

        Task AddRangeAsync(IEnumerable<T> entities);

        Task UpdateAsync(T entity);

        Task DeleteAsync(T entity);
    }
}