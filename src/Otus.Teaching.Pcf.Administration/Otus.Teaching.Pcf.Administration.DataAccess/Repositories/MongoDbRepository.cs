﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using MongoDB.Driver;
using Otus.Teaching.Pcf.Administration.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.Administration.Core.Domain;

namespace Otus.Teaching.Pcf.Administration.DataAccess.Repositories
{
    public class MongoDbRepository<T> : IRepository<T> where T : BaseEntity
    {
        private readonly IMongoCollection<T> _collection;
        
        public  MongoDbRepository(IMongoCollection<T> collection)
        {
            _collection = collection;
        }
        
        
        public async Task<IEnumerable<T>> GetAllAsync()
        {
            var items = await _collection.FindAsync(x => true).Result.ToListAsync();
            return items;
        }

        
        public Task<T> GetByIdAsync(Guid id)
        {
            var item = _collection.FindAsync(x => x.Id == id).Result.FirstOrDefaultAsync();
            return item;
        }

        
        public async Task<IEnumerable<T>> GetRangeByIdsAsync(List<Guid> ids)
        {
            var entities = await _collection.FindAsync(x => ids.Contains(x.Id)).Result.ToListAsync();
            return entities;
        }
        
        
        public async Task AddAsync(T entity)
        {
            await _collection.InsertOneAsync(entity);
        }

        
        public async Task AddRangeAsync(IEnumerable<T> entities)
        {
            await _collection.InsertManyAsync(entities);
        }

        
        public async Task UpdateAsync(T entity)
        {
            var filter = Builders<T>.Filter.Eq("Id", entity.Id.ToString());
            await _collection.ReplaceOneAsync(filter, entity);
        }

        
        public async Task DeleteAsync(T entity)
        {
            await _collection.FindOneAndDeleteAsync(x => x.Id == entity.Id);
        }
    }
}